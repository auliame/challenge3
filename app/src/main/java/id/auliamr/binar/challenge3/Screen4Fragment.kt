package id.auliamr.binar.challenge3

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import id.auliamr.binar.challenge3.databinding.FragmentScreen4Binding

class Screen4Fragment : Fragment() {

    private var _binding: FragmentScreen4Binding? = null
    private val binding get() = _binding!!

    private lateinit var name: String
    private lateinit var age: String
    private lateinit var address: String
    private lateinit var job: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentScreen4Binding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val dataUser = arguments?.getParcelable<DataUser>(Screen3Fragment.EXTRA_DATA) as DataUser

        name = dataUser.nama.toString()
        age = dataUser.usia.toString()
        address = dataUser.alamat.toString()
        job = dataUser.pekerjaan.toString()

        dataCheck()
        toScreen3()
    }

    private fun toScreen3() {
        binding.backScreen3.setOnClickListener{
            age = binding.inputAge.text.toString()
            address = binding.inputAddress.text.toString()
            job = binding.inputJob.text.toString()

            val dataUser = DataUser(name, age, address, job)
            val mBundle = bundleOf(Screen3Fragment.EXTRA_DATA to dataUser)
            it.findNavController()
                .navigate(R.id.action_screen4Fragment_to_screen3Fragment, mBundle)
        }
    }

    private fun dataCheck() {
        if (age != "null") {
            binding.inputAge.setText(age)
            binding.inputAddress.setText(address)
            binding.inputJob.setText(job)
        }
    }

}