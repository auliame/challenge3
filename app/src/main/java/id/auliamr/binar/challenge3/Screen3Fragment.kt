package id.auliamr.binar.challenge3

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import id.auliamr.binar.challenge3.databinding.FragmentScreen3Binding

class Screen3Fragment : Fragment() {

    private var _binding: FragmentScreen3Binding? = null
    private val binding get() = _binding!!

    private lateinit var name: String
    private lateinit var age: String
    private lateinit var address: String
    private lateinit var job: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentScreen3Binding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val userData = arguments?.getParcelable<DataUser>(EXTRA_DATA) as DataUser

        name = userData.nama.toString()
        age = userData.usia.toString()
        address = userData.alamat.toString()
        job = userData.pekerjaan.toString()

        dataCheck()
        toScreen4()
    }

    private fun toScreen4() {
        binding.BtntoScreen4.setOnClickListener{
            val dataUser = DataUser(name, age, address, job)
            val mBundle = bundleOf(EXTRA_DATA to dataUser)
            it.findNavController()
                .navigate(R.id.action_screen3Fragment_to_screen4Fragment, mBundle)
        }
    }

    private fun dataCheck() {
        if (age  == "null") {
            binding.viewName2.text = name
        } else {
            allData()
        }
    }

    private fun allData() {

        binding.apply {
            viewName.visibility = View.VISIBLE
            viewAge.visibility = View.VISIBLE
            viewAddress.visibility = View.VISIBLE
            viewJob.visibility = View.VISIBLE

            viewName2.text = ""
            viewName.text = name
            viewAge.text = oddEvenAge(age.toInt())
            viewAddress.text = address
            viewJob.text = job
        }
    }

    private fun oddEvenAge(age: Int): String {
        return if (age % 2 == 0) {
            "$age, bernilai Genap"
        } else {
            "$age, bernilai Ganjil"
        }
    }

    companion object {
        const val EXTRA_DATA = "extra_data"
    }
}