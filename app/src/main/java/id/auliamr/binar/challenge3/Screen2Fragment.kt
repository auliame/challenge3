package id.auliamr.binar.challenge3

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import id.auliamr.binar.challenge3.databinding.FragmentScreen2Binding

class Screen2Fragment: Fragment() {

    private var _binding: FragmentScreen2Binding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentScreen2Binding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.BtntoScreen3.setOnClickListener {
            val nama = binding.insertName.text.toString()
            val dataUser = DataUser(nama,null,null,null)
            val mBundle = bundleOf(Screen3Fragment.EXTRA_DATA to dataUser)
            it.findNavController().navigate(R.id.action_screen2Fragment_to_screen3Fragment, mBundle)
        }
    }



}